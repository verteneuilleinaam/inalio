document.addEventListener('DOMContentLoaded', () => {
  //list all cards
  const cardArray = [
    {
      name: 'time',
      img: 'gameimg/time.jpg',
      desc: 'Gestion du temps : J\'ai améliorer ma gestion du temps en travaillant sur plusieurs projets en même temps. J\'ai appris à prioriser les tâches mais il me reste du chemin.',
    },
    {
      name: 'equipe',
      img: 'gameimg/equipe.avif',
      desc: 'Travail en équipe: Je suis capable de travailler efficacement avec les autres pour atteindre un objectif commun. Je sais comment écouter les autres et partager mes idées.',
    },
    {
      name: 'discussion',
      img: 'gameimg/discussion.jpg',
      desc: 'Communication: J\'ai appris à communiquer efficacement avec les autres. Je sais comment exprimer mes idées clairement et écouter les autres, notamment dans le milieu professionel.',
    },
    {
      name: 'autonomie',
      img: 'gameimg/autonomie.avif',
      desc: 'Autonomie: Je suis capable de travailler de manière autonome et de prendre des décisions par moi-même. Je sais comment gérer mon temps et mes priorités.'
    },
    {
      name: 'time',
      img: 'gameimg/time.jpg',
      desc: 'Gestion du temps: J\'ai améliorer ma gestion du temps en travaillant sur plusieurs projets en même temps. J\'ai appris à prioriser les tâches mais il me reste du chemin.'
    },
    {
      name: 'equipe',
      img: 'gameimg/equipe.avif',
      desc: 'Travail en équipe : Je suis capable de travailler efficacement avec les autres pour atteindre un objectif commun. Je sais comment écouter les autres et partager mes idées.'
    },
    {
      name: 'discussion',
      img: 'gameimg/discussion.jpg',
      desc: 'Communication: J\'ai appris à communiquer efficacement avec les autres. Je sais comment exprimer mes idées clairement et écouter les autres, notamment dans le milieu professionelle.'
    },
    {
      name: 'autonomie',
      img: 'gameimg/autonomie.avif',
      desc: 'Autonomie : Je suis capable de travailler de manière autonome et de prendre des décisions par moi-même. Je sais comment gérer mon temps et mes priorités.'
    }
  ]

  cardArray.sort(() => 0.5 - Math.random())

  const grid = document.querySelector('.grid')
  const resultDisplay = document.querySelector('#result')
  let cardsChosen = []
  let cardsChosenId = []
  let cardsWon = []

  function createBoard() {
    for (let i = 0; i < cardArray.length; i++) {
      const card = document.createElement('img')
      card.setAttribute('src', 'gameimg/INVElogo.png')
      card.setAttribute('data-id', i)
      card.addEventListener('click', flipCard)
      grid.appendChild(card)
    }
  }

  //check for matches
  function checkForMatch() {
    const cards = document.querySelectorAll('img');
    const optionOneId = cardsChosenId[0];
    const optionTwoId = cardsChosenId[1];

    if (optionOneId == optionTwoId) {
      cards[optionOneId].setAttribute('src', 'gameimg/INVElogo.png');
      cards[optionTwoId].setAttribute('src', 'gameimg/INVElogo.png');
      alert('Vous avez cliqué sur la même image !');
    } else if (cardsChosen[0] === cardsChosen[1]) {
      const desc = cardArray[optionOneId].desc;
      alert(`Vous avez trouvé une paire ! ${desc}`);
      cardsWon.push(cardsChosen);
    } else {
      cards[optionOneId].setAttribute('src', 'gameimg/INVElogo.png');
      cards[optionTwoId].setAttribute('src', 'gameimg/INVElogo.png');
    }
    cardsChosen = [];
    cardsChosenId = [];
    resultDisplay.textContent = cardsWon.length;
    if (cardsWon.length === cardArray.length / 2) {
      resultDisplay.textContent = ' Félicitations ! Ces années ont été pour moi un moyen d\'apprendre et de progresser dans le domaine du développement. J\'ai pu développer de nouvelles compétences et améliorer mes compétences existantes. J\'ai hâte de continuer à apprendre et à progresser durant cette année 2024. Merci pour votre attention !'
    }
  }

  //flip card
  function flipCard() {
    let cardId = this.getAttribute('data-id')
    cardsChosen.push(cardArray[cardId].name)
    cardsChosenId.push(cardId)
    this.setAttribute('src', cardArray[cardId].img)
    if (cardsChosen.length === 2) {
      setTimeout(checkForMatch, 500)
    }
  }

  createBoard()
})
